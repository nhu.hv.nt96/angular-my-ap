import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component-overview',
  templateUrl: './component-overview.component.html',
  styleUrls: ['./component-overview.component.scss']
})
export class ComponentOverviewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log("component-overview excute ngOnInit")
    this.logIt(`OnInit`);
  }
  ngOnChanges(): void {
    console.log("component-overview excute ngOnChanges")
  }
  ngDoCheck(): void {
    console.log("component-overview excute ngDoCheck")
  }
  ngAfterContentInit(): void {
    console.log("component-overview excute ngAfterContentInit")
  }
  ngAfterContentChecked(): void {
    console.log("component-overview excute ngAfterContentChecked")
  }
  ngAfterViewInit(): void {
    console.log("component-overview excute ngAfterViewInit")
  }
  ngAfterViewChecked(): void {
    console.log("component-overview excute ngAfterViewChecked")
  }
  ngOnDestroy(): void {
    console.log("component-overview excute ngOnDestroy")
  }
  logIt(msg: string) {

  }
}
